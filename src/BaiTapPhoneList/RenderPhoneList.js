import React from "react";

export default function RenderPhoneList({
  dataPhone,
  handleShowDetail,
  handleAddToCart,
}) {
  return (
    <div className="row">
      {dataPhone.map((item) => {
        return (
          <div className="col-4">
            <img className="w-75" src={item.hinhAnh} alt="" />
            <h3>{item.tenSP}</h3>
            <button
              onClick={() => {
                handleShowDetail(item.maSP);
              }}
              className="btn btn-success me-3"
            >
              Watch details
            </button>
            <button
              onClick={() => {
                handleAddToCart(item);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        );
      })}
    </div>
  );
}
