import React from "react";
const vnd = (number) => {
  return (number * 1)

    .toLocaleString("it-IT", {
      style: "currency",
      currency: "VND",
    })
    .replace("VND", "₫");
};
export default function Cart({
  cart,
  handleRemoveProduct,
  handleChangeQuantity,
}) {
  return (
    <div>
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      >
        Your cart
      </button>
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" style={{ maxWidth: "1200px" }}>
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Your cart
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              />
            </div>
            <div className="modal-body">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <td>Tên sản phẩm</td>
                    <td>Hình ảnh</td>
                    <td>Giá</td>
                    <td>Số lượng</td>
                    <td>Tổng tiền</td>
                    <td>Thao tác</td>
                  </tr>
                </thead>
                <tbody>
                  {cart.map((item) => {
                    return (
                      <tr>
                        <td>{item.tenSP}</td>
                        <td>
                          <img className="w-25" src={item.hinhAnh} alt="" />
                        </td>
                        <td>{vnd(item.giaBan)}</td>
                        <td>
                          <button
                            onClick={() => {
                              handleChangeQuantity(item.maSP, 1);
                            }}
                            className="btn btn-success"
                          >
                            +
                          </button>
                          <span className="mx-2">{item.quantity}</span>
                          <button
                            onClick={() => {
                              handleChangeQuantity(item.maSP, -1);
                            }}
                            className="btn btn-secondary"
                          >
                            -
                          </button>
                        </td>
                        <td>{vnd(item?.quantity * item.giaBan)}</td>
                        <td>
                          <button
                            onClick={() => {
                              handleRemoveProduct(item.maSP);
                            }}
                            className="btn btn-danger"
                          >
                            Xóa
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <h3>
                {vnd(
                  cart.reduce((total, phone) => {
                    return (total += phone.giaBan * phone.quantity);
                  }, 0)
                )}
              </h3>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
