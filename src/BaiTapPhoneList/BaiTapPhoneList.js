import React, { useState } from "react";
import Cart from "./Cart";
import DetailPhone from "./DetailPhone";
import RenderPhoneList from "./RenderPhoneList";
import DataPhone from "./DataPhoneList.json";
export default function BaiTapPhoneList() {
  const [dataPhone, setData] = useState(DataPhone);
  const [detailPhone, setDetailPhone] = useState({});
  const [cart, setCart] = useState([]);

  const handleShowDetail = (maSP) => {
    let a = document.getElementById("chiTietSanPham");
    a.className = "container text-start mt-5 d-block";
    let index = dataPhone.findIndex((phone) => {
      return phone.maSP === maSP;
    });
    setDetailPhone(dataPhone[index]);
  };

  const handleAddToCart = (product) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((phone) => {
      return phone.maSP === product.maSP;
    });
    if (index == -1) {
      cloneCart.push({ ...product, quantity: 1 });
    } else {
      cloneCart[index].quantity++;
    }
    setCart(cloneCart);
  };

  const handleRemoveProduct = (maSP) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((phone) => {
      return phone.maSP === maSP;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };

  const handleChangeQuantity = (maSP, value) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((phone) => {
      return phone.maSP === maSP;
    });
    if (index !== -1) {
      cloneCart[index].quantity += value;
    }
    if (cloneCart[index].quantity === 0) {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };

  return (
    <div>
      <Cart
        cart={cart}
        handleRemoveProduct={handleRemoveProduct}
        handleChangeQuantity={handleChangeQuantity}
      />
      <RenderPhoneList
        dataPhone={dataPhone}
        handleShowDetail={handleShowDetail}
        handleAddToCart={handleAddToCart}
      />
      <DetailPhone detailPhone={detailPhone} />
    </div>
  );
}
