import React from "react";

export default function DetailPhone({ detailPhone }) {
  let {
    hinhAnh,
    maSP,
    tenSP,
    giaBan,
    ram,
    rom,
    heDieuHanh,
    manHinh,
    cameraTruoc,
    cameraSau,
  } = detailPhone;
  return (
    <div className="container text-start mt-5 d-none" id="chiTietSanPham">
      <div className="row">
        <div className="col-4 h-100">
          <h1>{tenSP}</h1>
          <img className="w-100" src={hinhAnh} alt="" />
        </div>
        <div className="col-8 text-left">
          <table className="table">
            <thead>
              <tr>
                <td colSpan={2}>
                  <h3>Chi tiết sản phẩm</h3>
                </td>
              </tr>
              <tr>
                <td>Màn hình</td>
                <td>{manHinh}</td>
              </tr>
              <tr>
                <td>Hệ điều hành</td>
                <td>{heDieuHanh}</td>
              </tr>
              <tr>
                <td>Camera trước</td>
                <td>{cameraTruoc}</td>
              </tr>
              <tr>
                <td>Camera sau</td>
                <td>{cameraSau}</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>{ram}</td>
              </tr>
              <tr>
                <td>ROM</td>
                <td>{rom}</td>
              </tr>
              <tr>
                <td>Giá bán</td>
                <td>{giaBan + "đ"}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
}
