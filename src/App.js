import logo from "./logo.svg";
import "./App.css";
import BaiTapPhoneList from "./BaiTapPhoneList/BaiTapPhoneList";

function App() {
  return (
    <div className="App">
      <BaiTapPhoneList />
    </div>
  );
}

export default App;
